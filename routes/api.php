<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\FrontendController;
use App\Http\Controllers\api\CategoryController;
use App\Http\Controllers\api\ProductController;
use App\Http\Controllers\api\OrderController;
use App\Http\Controllers\api\CartController;


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Home Screen

// Route::group(['middleware' => ['api']], function () {
//   Route::post('user/logoutUser' ,[FrontendController::class,'logout']);
// });

// Route::group(['middleware' => ['web', 'throttle:60,1']], function () {
//    Route::post('api/user/registerUser', [FrontendController::class,'registerSubmit'])->name('register');
// });

// Route::middleware('auth:sanctum')->group(function () {
//     Route::post('user/logoutUser' ,[FrontendController::class,'logout']);
// });

    
 Route::post('user/logoutUser' ,[FrontendController::class,'logout']);
Route::post('user/login', [FrontendController::class, 'loginSubmit']);
Route::get('api/user/getUser' ,[FrontendController::class,'getUser']);
Route::post('user/registerUser', [FrontendController::class,'apiRegisterSubmit']);
Route::get('user/ListSlide', [FrontendController::class,'ListSlide']);
 //Route::post('user/logoutUser' ,[FrontendController::class,'logout']);

Route::get('api/listCategory' ,[CategoryController::class,'ListCategory']);
Route::get('api/get-product-by_cateId/{cat_id}',[ProductController::class,'filterProduct']);

Route::get('api/listProduct' ,[ProductController::class,'ListProducts']);
Route::get('detailProduct/{id}' ,[ProductController::class,'detailProduct']);
Route::get("api/order/listOrder" , [OrderController::class, 'listOrder']);

Route::get('api/cart/list-cart', [CartController::class, 'listCart']);

Route::post('api/cart/update-cart', [CartController::class, 'cartUpdate']);

Route::post('cart/add-to-cart', [CartController::class, 'addToCart']);