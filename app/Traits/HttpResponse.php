<?php

namespace App\Traits;

trait HttpResponse {
    protected function success($data,$message = null,$code = 200){
        return response()->json([
            "status" => $code,
            "data" => $data,
            "message" => $message,
        ], $code);
    }

    protected function error($message = null,$code = 500){
        return response()->json([
            "status" => $code,
            "message" => $message,
        ], $code);
    }
}
