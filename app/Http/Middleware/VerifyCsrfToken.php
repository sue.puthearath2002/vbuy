<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
          //'api/user/login',
         //'api/user/registerUser',
         'api/user/banerIndex',
         'api/get-product-slug',
         'api/cart/update-cart',
         'api/cart/add-to-cart', 
         
    ];
}