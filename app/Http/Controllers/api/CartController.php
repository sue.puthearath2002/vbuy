<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Product;
use Auth;
use Helper;

class CartController extends Controller
{
    protected $product = null;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function listCart()
    {
        $order = Cart::whereNull('order_id')->get();
        if ($order) {
            return response()->json(
                [
                    'status' => 200,
                    'record' => $order,
                ],
                200,
            );
        } else {
            return response()->json(
                [
                    'status' => 404,
                    'err' => 'No record',
                ],
                404,
            );
        }
    }

    public function addToCart(Request $request)
    {
      
        $request->validate([
            'slug' => 'required|exists:products,slug',
        ]);
            $user_id = Auth()->user();
        // if (Auth::check()) {
          
        //     $user_id = Auth()->user()->id;

        //     return $user_id;
        // } else {
        //     // If user is not authenticated, return 0 or any other appropriate value
        //     return 0;
        // }
return $user_id ;
        $product = Product::where('slug', $request->slug)->first();
        //  return auth()->user()->id;

        if (!$product) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Product not found',
                ],
                404,
            );
        }

        // Check if the product is already in the cart for the authenticated user
        $cartItem = Cart::where('user_id', auth()->user()->id)
            ->where('product_id', $product->id)
            ->whereNull('order_id')
            ->first();

        if ($cartItem) {
            // If the product is already in the cart, update the quantity
            $cartItem->quantity += 1;
            $cartItem->save();
        } else {
            // If the product is not in the cart, add a new item
            $cartItem = new Cart();
            $cartItem->user_id = auth()->user()->id;
            $cartItem->product_id = $product->id;
            $cartItem->price = $product->price - ($product->price * $product->discount) / 100;
            $cartItem->quantity = 1;
            $cartItem->amount = $cartItem->price * $cartItem->quantity;
            if ($cartItem->product->stock < $cartItem->quantity || $cartItem->product->stock <= 0) {
                return response()->json(
                    [
                        'status' => 'wuh!',
                        'message' => 'Stock not sufficient!.',
                        'cart_item' => $cartItem,
                    ],
                    200,
                );
            }
            $cartItem->save();
        }

        return response()->json(
            [
                'status' => 'success',
                'message' => 'Product added to cart successfully',
                'cart_item' => $cartItem,
            ],
            200,
        );
    }

    public function cartUpdate(Request $request)
    {
        $id = $request->id;
        $newQty = $request->qty;

        if ($newQty) {
            $cartId = Cart::find($id);
            if (!$cartId) {
                return response()->json(
                    [
                        'status' => 404,
                        'message' => 'Cart item not found',
                    ],
                    404,
                );
            }

            if ($cartId->product->stock < $newQty) {
                return response()->json(
                    [
                        'status' => 200,
                        'message' => 'out of Stock!',
                    ],
                    200,
                );
            } else {
                $cartId->quantity = $cartId->product->stock > $newQty ? $newQty : $cartId->product->stock;
                $after_price = $cartId->product->price - ($cartId->product->price * $cartId->product->discount) / 100;
                $cartId->amount = $after_price * $newQty;

                $cartId->save();
                return response()->json(
                    [
                        'status' => 200,
                        'message' => 'Cart quantity updated successfully',
                        'cart_id' => $cartId,
                    ],
                    200,
                );
            }
        } else {
            return response()->json(
                [
                    'status' => 404,
                    'msg' => 'Cart Invalid!',
                ],
                404,
            );
        }
    }
}