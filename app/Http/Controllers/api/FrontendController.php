<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Product;
use App\Models\Category;
use App\Models\PostTag;
use App\Models\PostCategory;
use App\Models\Post; 
use App\Models\Cart;
use App\Models\Brand;
use App\User;
use Illuminate\Support\Facades\Validator;
use Auth;
// use Session;
// use Newsletter;
use DB;
use Hash;



class FrontendController extends Controller
{
  public function loginSubmit(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        if($validate->fails()){
            return response()->json([
                'status' => 'failed',
                'message' => 'Validation Error!',
                'data' => $validate->errors(),
            ], 403);  
        }

        // Check email exist
        $user = User::where('email', $request->email)->first();

        // Check password
        if(!$user || !Hash::check($request->password, $user->password)) {
            return response()->json([
                'status' => 'failed',
                'message' => 'Invalid credentials'
                ], 401);
        }

        $data['token'] = $user->createToken($request->email)->plainTextToken;
        $data['user'] = $user;
        
        $response = [
            'status' => 'success',
            'message' => 'User is logged in successfully.',
            'data' => $data,
        ];

        return response()->json($response, 200);
    } 
    
public function logout (Request $request) {
        $accessToken = Auth()->user()->token();
        $token = $request->user()->tokens->find($accessToken);
        $token->revoke();

        return response([
            'message' => 'You have been successfully logged out.',
        ], 200);
    }     


    
   

   public function getUser()
    {
    $user = User::all();

    if ($user->count()>0) {
        return response()->json([
            "status" => 200,
            'record' => $user,
        ], 200);
    } else {
        return response()->json([
            "status" => 404,
            'err' => "Invalid email and password, please try again!",
        ], 404);
    }
   }

   
    public function apiRegisterSubmit(Request $request)
    {
        
        $request->validate([
            'name' => 'required|string|min:2',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                
            ]);
            if($user){
                return response()->json([
                                "status" => 200,
                                "data" => $user,
                                "message" => "User created successfully",
                                "token"=> $user-> createToken('API Token of' . $user->name)->plainTextToken
                            ], 200,);
            }else{

            }
            return response()->json([
                            "status" => 500,
                            "message" => "Failed to create user. Please try again.",
                            "error" => $e->getMessage(), 
                        ], 500);
        
    }

    public function ListSlide()
    {
        $banner=Banner::all();
        return  response()->json([
                "status" => 200,
                "data" => $banner,
                "message" => "successfully",
            ], 200);
    }
        
}