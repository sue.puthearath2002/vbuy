<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;

class OrderController extends Controller
{
   public function listOrder(){
     $order = Order::all();
     if ($order) {
        return response()->json([
            "status" => 200,
            'record' => $order,
        ], 200);
    } else {
        return response()->json([
            "status" => 404,
            'err' => "No record",
        ], 404);
    }
   } 
}
