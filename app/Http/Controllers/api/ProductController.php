<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
     public function ListProducts()
    {
         //$category=Category::orderBy('id','DESC')->paginate(10);
        $category=Product::all();
        if($category){
return  response()->json([
                "status" => 200,
                "data" => $category,
                "message" => "successfully",
            ], 200);
        }else{
           return response()->json([
            "status" => 404,
            "message" => "Product not found",
        ], 404); 
        }
        
    }

    public function filterProduct($cateID)
{

    $product = Product::where('cat_id', $cateID)->get(); 

    if ($product) {
        return response()->json([
            "status" => 200,
            "data" => $product,
            "message" => "Product found successfully",
        ], 200);
    } else {
        return response()->json([
            "status" => 404,
            "message" => "Product not found",
        ], 404);
    }
}

public function detailProduct(Request $request){
    $product = Product::where('id', $request->id)->first();

    if ($product) {
        return response()->json([
            "status" => 200,
            "data" => $product,
            "message" => "Product found successfully",
        ], 200);
    } else {
        return response()->json([
            "status" => 404,
            "message" => "Product not found",
        ], 404);
    }
}

}
